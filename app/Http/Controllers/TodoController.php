<?php

namespace App\Http\Controllers;

use App\todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todo_create');

        // return redirect('todo_show');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $res=new todo;
        $res->name=$request->input('name');
        $res->save();


        // message
        $request->session()->flash('msg','record inserted');

        // redirect to home
        return redirect('todo_show');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(todo $todo)
    {
        
        return view('todo_show')->with('todoArr',todo::all());


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(todo $todo,$id)
    {
        return view('todo_edit')->with('todoArr',todo::find($id));

      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, todo $todo)
    {
        $res=todo::find($request->id);
        $res->name=$request->input('name');
        $res->save();

           // message
           $request->session()->flash('msg','Record Updated');

           // redirect to home
           return redirect('todo_show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(todo $todo,$id)
    {
         todo::destroy(array('id',$id));
         return redirect('todo_show');
    }
}
