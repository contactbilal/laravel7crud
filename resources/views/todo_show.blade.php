<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Laravel</title>
      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <!-- Styles -->
      <style>
      </style>
   </head>
   <body>
      <div class="container">
         <div class="row">
            <div class="col-md-8 mx-auto">
               <div class="alert" role="alert">
                  <strong>{{ session('msg') }}</strong>
               </div>
                <div class="my-4">
                    <a href="todo_create" class="btn btn-info">Add Record</a>
                </div>
               <table class="table table-bordered">
                  <thead>
                     <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Name</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Action</th>
                     </tr>
                  </thead>
                  <tbody>
                      @foreach ($todoArr as $todo)
                      <tr>
                        <td>{{ $todo->id }}</td>
                           <td>{{ $todo->name }}</td>
                           <td>{{ $todo->created_at }}</td>
                           <td>
                              <a href="todo_delete/{{$todo->id}}" class="btn btn-primary pr-2">Delete</a>
                              <a href="todo_edit/{{$todo->id}}" class="btn btn-success">Edit</a>
                           </td>
                        </tr>
                      @endforeach
                   
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </body>
</html>