<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Laravel</title>
      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <!-- Styles -->
      <style>
      </style>
   </head>
   <body>
      <div class="container">
         <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="my-2">
                    <a href="todo_show" class="btn btn-info">Back Home</a>
                </div>
                <form  method="post" action="todo_submit">
                    @csrf
               <table class="table table-bordered">
               
                  <tbody>
                   
                          
                        <tr>
                            <th scope="col">Name</th>
                            <td>
                                <input type="textname" class="form-control" name="name" id="name" required>
                                <div class="text-center mt-2">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                               
                            </td>
                              
                        </tr>
                       
                     
                  </tbody>
               </table>
            </form>
            </div>
         </div>
      </div>
   </body>
</html>